variable "gitlab_api_token" {
  type        = string
  description = "The API token for creating GitLab deploy tokens (https://docs.gitlab.com/ee/api/rest/#authentication)"
  sensitive   = true
}

variable "flux_cluster_base_gitlab_project_id" {
  type        = number
  description = "The GitLab project ID of the Flux cluster base repository"
}

variable "openstack_config" {
  type = object({
    auth_url                      = optional(string)
    cloud                         = optional(string)
    region                        = optional(string)
    user_name                     = optional(string)
    user_id                       = optional(string)
    application_credential_id     = optional(string)
    application_credential_name   = optional(string)
    application_credential_secret = optional(string)
    tenant_id                     = optional(string)
    tenant_name                   = optional(string)
    password                      = optional(string)
    token                         = optional(string)
    user_domain_name              = optional(string)
    user_domain_id                = optional(string)
    project_domain_name           = optional(string)
    project_domain_id             = optional(string)
    domain_id                     = optional(string)
    domain_name                   = optional(string)
    default_domain                = optional(string)
    system_scope                  = optional(string)
    insecure                      = optional(string)
    cacert_file                   = optional(string)
    cert                          = optional(string)
    key                           = optional(string)
    endpoint_type                 = optional(string)
    endpoint_overrides            = optional(map(string))
    swauth                        = optional(string)
    use_octavia                   = optional(string)
    disable_no_cache_header       = optional(string)
    delayed_auth                  = optional(string)
    allow_reauth                  = optional(string)
    max_retries                   = optional(string)
    enable_logging                = optional(string)
  })
  description = "OpenStack config for provider details"
}

variable "base_name" {
  type        = string
  description = "Prefix added to names of all OpenStack resources"
  default     = ""
}

variable "openstack_flavor_name" {
  type        = string
  description = "Compute instance flavor name used for all compute instances"
  default     = "Standard 8GB"
}

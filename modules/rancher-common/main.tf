locals {
  rancher_kubernetes_version = "v1.24.14+k3s1"
}

# https://registry.terraform.io/providers/loafoe/ssh/latest/docs/resources/resource
resource "ssh_resource" "install_k3s" {
  host = var.node_public_ip
  commands = [
    "curl https://get.k3s.io | INSTALL_K3S_EXEC=\"server --node-external-ip=${var.node_public_ip} --node-ip=${var.node_internal_ip} --disable=traefik\" INSTALL_K3S_VERSION=${local.rancher_kubernetes_version} sh -"
  ]
  user        = var.node_username
  private_key = var.ssh_private_key_pem
}

# https://registry.terraform.io/providers/loafoe/ssh/latest/docs/resources/resource
resource "ssh_resource" "retrieve_config" {
  depends_on = [
    ssh_resource.install_k3s
  ]
  host = var.node_public_ip
  commands = [
    "sudo cat /etc/rancher/k3s/k3s.yaml"
  ]
  user        = var.node_username
  private_key = var.ssh_private_key_pem
}

locals {
  kube_config_yaml = replace(ssh_resource.retrieve_config.result, "127.0.0.1", var.node_public_ip)
  kube_config      = yamldecode(local.kube_config_yaml)
}

output "rancher_url" {
  value       = "https://${var.rancher_server_dns}"
  description = "The URL used for the Rancher web application"
}

output "kube_config_content" {
  value       = local.kube_config_yaml
  description = "The contents of Kubernetes config file to connect to the cluster"
  sensitive   = true
}

output "cluster_api_endpoint" {
  value       = local.kube_config.clusters[0].cluster.server
  description = "The HTTPS endpoint of the cluster"
}

output "cluster_ca_certificate" {
  value       = base64decode(local.kube_config.clusters[0].cluster.certificate-authority-data)
  description = "The CA certificate of the cluster"
}

output "cluster_client_certificate" {
  value       = base64decode(local.kube_config.users[0].user.client-certificate-data)
  description = "The client certificate of the cluster"
}

output "cluster_client_key" {
  value       = base64decode(local.kube_config.users[0].user.client-key-data)
  description = "The client key of the cluster"
}

output "rancher_url" {
  value       = module.rancher_common.rancher_url
  description = "The URL used for the Rancher web application"
}

output "rancher_node_ip" {
  value       = openstack_compute_instance_v2.rancher_server.access_ip_v4
  description = "The IPv4 address of the node Rancher is running on"
}

output "ssh_key_pair_name" {
  value       = openstack_compute_keypair_v2.ssh_key.name
  description = "The generated SSH key pair name in OpenStack"
}

output "ssh_private_key_contents" {
  value       = tls_private_key.global_key.private_key_pem
  description = "The generated SSH private key contents"
  sensitive   = true
}

# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs
provider "gitlab" {
  token = var.gitlab_api_token
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs
provider "openstack" {
  auth_url                      = var.openstack_config.auth_url
  cloud                         = var.openstack_config.cloud
  region                        = var.openstack_config.region
  user_name                     = var.openstack_config.user_name
  user_id                       = var.openstack_config.user_id
  application_credential_id     = var.openstack_config.application_credential_id
  application_credential_name   = var.openstack_config.application_credential_name
  application_credential_secret = var.openstack_config.application_credential_secret
  tenant_id                     = var.openstack_config.tenant_id
  tenant_name                   = var.openstack_config.tenant_name
  password                      = var.openstack_config.password
  token                         = var.openstack_config.token
  user_domain_name              = var.openstack_config.user_domain_name
  user_domain_id                = var.openstack_config.user_domain_id
  project_domain_name           = var.openstack_config.project_domain_name
  project_domain_id             = var.openstack_config.project_domain_id
  domain_id                     = var.openstack_config.domain_id
  domain_name                   = var.openstack_config.domain_name
  default_domain                = var.openstack_config.default_domain
  system_scope                  = var.openstack_config.system_scope
  insecure                      = var.openstack_config.insecure
  cacert_file                   = var.openstack_config.cacert_file
  cert                          = var.openstack_config.cert
  key                           = var.openstack_config.key
  endpoint_type                 = var.openstack_config.endpoint_type
  endpoint_overrides            = var.openstack_config.endpoint_overrides
  swauth                        = var.openstack_config.swauth
  use_octavia                   = var.openstack_config.use_octavia
  disable_no_cache_header       = var.openstack_config.disable_no_cache_header
  delayed_auth                  = var.openstack_config.delayed_auth
  allow_reauth                  = var.openstack_config.allow_reauth
  max_retries                   = var.openstack_config.max_retries
  enable_logging                = var.openstack_config.enable_logging
}

# https://registry.terraform.io/providers/fluxcd/flux/latest/docs
provider "flux" {
  kubernetes = {
    host                   = module.rancher_common.cluster_api_endpoint
    cluster_ca_certificate = module.rancher_common.cluster_ca_certificate
    client_certificate     = module.rancher_common.cluster_client_certificate
    client_key             = module.rancher_common.cluster_client_key
  }
  git = {
    url = "ssh://${replace(data.gitlab_project.this.ssh_url_to_repo, ":", "/")}"
    ssh = {
      username    = split("@", data.gitlab_project.this.ssh_url_to_repo)[0]
      private_key = tls_private_key.this.private_key_pem
    }
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs
provider "kubernetes" {
  host                   = module.rancher_common.cluster_api_endpoint
  cluster_ca_certificate = module.rancher_common.cluster_ca_certificate
  client_certificate     = module.rancher_common.cluster_client_certificate
  client_key             = module.rancher_common.cluster_client_key
}

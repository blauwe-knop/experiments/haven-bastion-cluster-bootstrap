# https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key
resource "tls_private_key" "global_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

# https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/sensitive_file
resource "local_sensitive_file" "ssh_private_key_pem" {
  filename        = "${path.module}/id_rsa"
  content         = tls_private_key.global_key.private_key_pem
  file_permission = "0600"
}

# https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file
resource "local_file" "ssh_public_key_openssh" {
  filename = "${path.module}/id_rsa.pub"
  content  = tls_private_key.global_key.public_key_openssh
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/pet
resource "random_pet" "base_name" {
  count = var.base_name == "" ? 1 : 0
}

locals {
  base_name = var.base_name == "" ? random_pet.base_name[0].id : var.base_name
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id
resource "random_id" "ssh_key_name" {
  prefix      = "${local.base_name}-ssh-key-"
  byte_length = 4
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_keypair_v2
resource "openstack_compute_keypair_v2" "ssh_key" {
  name       = random_id.ssh_key_name.hex
  public_key = tls_private_key.global_key.public_key_openssh
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/data-sources/images_image_v2
data "openstack_images_image_v2" "ubuntu" {
  name        = "Ubuntu 22.04 (LTS)"
  most_recent = true
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/data-sources/networking_secgroup_v2
data "openstack_networking_secgroup_v2" "allow_web" {
  name = "allow-web"
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/data-sources/networking_secgroup_v2
data "openstack_networking_secgroup_v2" "remote_access" {
  name = "remote-access"
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id
resource "random_id" "security_group_kubernetes_api_name" {
  prefix      = "${local.base_name}-kubernetes-api-"
  byte_length = 4
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_secgroup_v2
resource "openstack_networking_secgroup_v2" "kubernetes_api" {
  name        = random_id.security_group_kubernetes_api_name.hex
  description = "Enables ingress to the Kubernetes API"
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_secgroup_rule_v2
resource "openstack_networking_secgroup_rule_v2" "kubernetes_api" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6443
  port_range_max    = 6443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.kubernetes_api.id
}

# https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id
resource "random_id" "rancher_server_name" {
  prefix      = "${local.base_name}-rancher-server-"
  byte_length = 4
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_instance_v2
resource "openstack_compute_instance_v2" "rancher_server" {
  name        = random_id.rancher_server_name.hex
  image_id    = data.openstack_images_image_v2.ubuntu.id
  flavor_name = var.openstack_flavor_name
  key_pair    = openstack_compute_keypair_v2.ssh_key.name

  security_groups = [
    data.openstack_networking_secgroup_v2.allow_web.name,
    data.openstack_networking_secgroup_v2.remote_access.name,
    openstack_networking_secgroup_v2.kubernetes_api.name,
  ]

  network {
    name = "net-public"
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'Waiting for cloud-init to complete...'",
      "cloud-init status --wait > /dev/null",
      "echo 'Completed cloud-init!'",
    ]

    connection {
      type        = "ssh"
      host        = self.access_ip_v4
      user        = "ubuntu"
      private_key = tls_private_key.global_key.private_key_pem
    }
  }
}

locals {
  base_domain_name = join(".", [openstack_compute_instance_v2.rancher_server.access_ip_v4, "sslip.io"])
}

module "rancher_common" {
  source = "./modules/rancher-common"

  node_public_ip      = openstack_compute_instance_v2.rancher_server.access_ip_v4
  node_internal_ip    = openstack_compute_instance_v2.rancher_server.access_ip_v4
  node_username       = "ubuntu"
  ssh_private_key_pem = tls_private_key.global_key.private_key_pem

  rancher_server_dns = join(".", ["rancher", local.base_domain_name])
}

# https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file
resource "local_file" "kube_config" {
  filename = format("%s/%s", path.root, "kubeconfig.yaml")
  content  = module.rancher_common.kube_config_content
}

# https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key
resource "tls_private_key" "this" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project
data "gitlab_project" "this" {
  id = var.flux_cluster_base_gitlab_project_id
}

# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_key
resource "gitlab_deploy_key" "this" {
  project  = data.gitlab_project.this.id
  title    = "Cluster bootstrap"
  key      = tls_private_key.this.public_key_openssh
  can_push = true
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace_v1
resource "kubernetes_namespace_v1" "cattle_system" {
  metadata {
    name = "cattle-system"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map_v1
resource "kubernetes_config_map_v1" "cattle_system_parameters" {
  metadata {
    name      = "cattle-system-parameters"
    namespace = kubernetes_namespace_v1.cattle_system.metadata[0].name
  }

  data = {
    rancher-server-host = join(".", ["rancher", local.base_domain_name])
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map_v1
resource "kubernetes_config_map_v1" "cattle_system_values" {
  metadata {
    name      = "cattle-system-values"
    namespace = kubernetes_namespace_v1.cattle_system.metadata[0].name
  }

  data = {
    "values.yaml" = yamlencode({})
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace_v1
resource "kubernetes_namespace_v1" "flux_system" {
  metadata {
    name = "flux-system"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map_v1
resource "kubernetes_config_map_v1" "flux_system_parameters" {
  metadata {
    name      = "flux-system-parameters"
    namespace = kubernetes_namespace_v1.flux_system.metadata[0].name
  }

  data = {}
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map_v1
resource "kubernetes_config_map_v1" "flux_system_values" {
  metadata {
    name      = "flux-system-values"
    namespace = kubernetes_namespace_v1.flux_system.metadata[0].name
  }

  data = {
    "values.yaml" = yamlencode({
      ingress = {
        hosts = [
          {
            host = join(".", ["flux-ui", local.base_domain_name])
            paths = [
              {
                path     = "/"
                pathType = "ImplementationSpecific"
              }
            ]
        }]
        tls = [
          {
            secretName = "flux-ui-tls"
            hosts = [
              join(".", ["flux-ui", local.base_domain_name])
            ]
          }
        ]
      }
    })
  }
}

# https://registry.terraform.io/providers/fluxcd/flux/latest/docs/resources/bootstrap_git
resource "flux_bootstrap_git" "this" {
  depends_on = [
    kubernetes_config_map_v1.cattle_system_values,
    kubernetes_config_map_v1.cattle_system_parameters,
    kubernetes_config_map_v1.flux_system_values,
    kubernetes_config_map_v1.flux_system_parameters,
  ]
  path = "clusters/demo"
}

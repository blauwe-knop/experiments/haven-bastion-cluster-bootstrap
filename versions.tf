terraform {
  required_version = ">= 1.6"
  required_providers {
    flux = {
      source  = "fluxcd/flux"
      version = "1.1.1"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.3.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.23.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.4.0"
    }
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.52.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.5.1"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.4"
    }
  }
}
